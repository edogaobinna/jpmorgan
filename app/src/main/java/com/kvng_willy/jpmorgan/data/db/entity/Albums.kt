package com.kvng_willy.jpmorgan.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Date

@Entity
data class Albums(
    @PrimaryKey(autoGenerate = true)
    val uuid:Int?,
    var userId: Int?,
    var id: Int?,
    var title: String?,
)
