package com.kvng_willy.jpmorgan.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kvng_willy.jpmorgan.data.db.entity.Albums

@Dao
interface AlbumDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(albums: Albums) : Long

    @Query("SELECT * FROM Albums")
    fun getAlbums() : LiveData<List<Albums>>

    @Query("DELETE FROM Albums WHERE id=:uid")
    suspend fun delete(uid:Int)
}