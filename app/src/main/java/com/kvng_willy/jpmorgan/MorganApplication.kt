package com.kvng_willy.jpmorgan

import androidx.multidex.MultiDexApplication
import com.kvng_willy.jpmorgan.data.db.AppDatabase
import com.kvng_willy.jpmorgan.data.network.MyApi
import com.kvng_willy.jpmorgan.data.network.NetworkConnectionInterceptor
import com.kvng_willy.jpmorgan.data.repository.DataRepository
import com.kvng_willy.jpmorgan.ui.ViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MorganApplication:MultiDexApplication(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MorganApplication))
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { DataRepository(instance(),instance()) }
        bind() from provider { ViewModelFactory(instance()) }
    }

}