package com.kvng_willy.jpmorgan.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import com.kvng_willy.jpmorgan.data.db.entity.Albums
import com.kvng_willy.jpmorgan.data.network.responses.AlbumResponse
import com.kvng_willy.jpmorgan.data.repository.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ViewModel(
    private val repository: DataRepository
):ViewModel() {

    fun fetchAlbums():ArrayList<AlbumResponse> {
        return repository.fetchAlbums()
    }
    var albums = repository.getAlbums()
}