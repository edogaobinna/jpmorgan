package com.kvng_willy.jpmorgan.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.jpmorgan.data.repository.DataRepository

@Suppress("UNCHECKED_CAST")
class ViewModelFactory (
    private val repository: DataRepository
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ViewModel(repository) as T
        }
}